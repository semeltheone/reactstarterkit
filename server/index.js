var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.set('port', process.env.PORT || 1812);

app.use(express.static('./../client/'));
app.use(bodyParser.json());

var server = app.listen(app.get('port'), () => {
  console.log(`Server is running on port ${app.get('port')}`);
});
