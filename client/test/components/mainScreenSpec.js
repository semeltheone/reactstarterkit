import React from 'react';
import {shallow, mount} from 'enzyme';
import MainScreen from '../../components/mainScreen.js'


export default describe('<MainScreen/>', () => {
  it('should render the given text in the main screen component', () => {
    let wrapper = mount(<MainScreen text="Test"></MainScreen>)
    expect(wrapper.find("h1").text()).to.equal('Test');
  });
});