module.exports = {
  devtool: 'source-map',
  entry: {
    main: './client/app.js',
    test: './client/test/index.js',
  },
  output: {
    path: __dirname,
    filename: './client/[name]bundle.js',
  },
  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  module: {
    loaders: [
      {

        loader: 'babel-loader',
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        query: {

          presets: ['es2015', 'react'],
        },
      }, {
        test: /\.json$/,
        loader: 'json',
      },
    ],
  },
};
