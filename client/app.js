const React = require('react');
const ReactDOM = require('react-dom');
import MainScreen from './components/mainScreen.js'

ReactDOM.render(<MainScreen text="Hello from React starter kit"/>,
    document.getElementById('app')
);