const React = require('react');

export default React.createClass({
  propTypes: {
    text:React.PropTypes.string,
  },

  render: function () {
    return (
        <div>
        <h1>{this.props.text}</h1>
        </div>
    );

  },
});